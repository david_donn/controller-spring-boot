package org.bitbucket.kilda.controller;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import org.apache.zookeeper.KeeperException;
//import org.apache.zookeeper.WatchedEvent;
//import org.apache.zookeeper.Watcher;
//import org.apache.zookeeper.ZooKeeper;

// NB: Using log4j directly in Main to be able to print out some information that is
//		not available in the SLF4J API (ie get log level)
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

/**
 * Main is the class that kicks off the controller.
 *
 * The controller starts with reaching out to Zookeeper. That is a critical
 * piece of the platform and must be in place for anything real to happen.
 *
 */
@SpringBootApplication
@ComponentScan
@Component
public class Main implements ApplicationListener<ApplicationReadyEvent> {
	private static final Logger logger = LoggerFactory.getLogger(Main.class);
	private static Context context;
	
	private static Heartbeat.Consumer hbc;
	private static Heartbeat.Producer hbp;

	public static final int ZK_DEFAULT_PORT = 2181;
	public static final int KAFKA_DEFAULT_PORT = 9092;
	
	@Value("${logging.level.root}")
	private String logLevel;
	
	@Autowired
	private Heartbeat hb;

	private void Initialize(String[] args) {
		Thread.currentThread().setName("Kilda.Main");
		logger.info("INITIALIZING Kilda");
		logger.info("logger level: " + logLevel);
		logger.debug("args count: " + args.length);
		logger.debug("args value: " + Arrays.toString(args));
		// NB: This is a poor mans config parser. It should move to something
		// more robust.
		String configfile = "";
		for (String arg : args) {
			if (arg.startsWith("--config")) {
				configfile = arg.substring(arg.lastIndexOf("=") + 1);
			}
		}
		logger.debug("configfile: " + configfile);
		context = new Context(configfile);
	}

	private void Startup() {
		logger.info("STARTING Kilda..");
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				Main.Shutdown();
			}
		});
		hbp = hb.createProducer();
		hbc = hb.createConsumer();
		hbp.run();
		hbc.run();
	}

	/**
	 * NB: Currently, this isn't called during a normal exit, only during a
	 * shutdown event.
	 */
	private static void Shutdown() {
		logger.info("EXITING Kilda");
		// Thread.
	}
	
    public static void main(String[] args) throws Exception {
        SpringApplication.run(Main.class, args);
    }

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		try {
			Initialize(event.getArgs());
			Startup();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
}

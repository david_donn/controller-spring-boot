package org.bitbucket.kilda.controller;

import java.time.Instant;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.Executors;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

/**
 * Heartbeat is a simple mechanism for monitoring health.
 * * It sends a record to a kafka topic every period (the period is configurable).
 *
 * REFERENCES - Producer:
 * https://kafka.apache.org/0101/javadoc/index.html?org/apache/kafka/clients/producer/KafkaProducer.html
 * - Consumer:
 * https://kafka.apache.org/0101/javadoc/index.html?org/apache/kafka/clients/consumer/KafkaConsumer.html
 */
@Component
public class Heartbeat {

	private static final Logger logger = LoggerFactory.getLogger(Heartbeat.class);
	
	private final Instant startTime;
	
	@Value("${kafka.host}")
	private String host;
	
	@Value("${kafka.port}")
	private Integer port;

	@Value("${ops.heartbeat.topic:kilda.heartbeat}")
	private String topicName;
	
	@Value("${ops.heartbeat.listener.sleep:5000}")
	private Long sleepTime;

	@Value("${ops.heartbeat.listener.group.id:kilda.heartbeat.listener}")
	private String groupId;
	
	@Value("${ops.heartbeat.listener.commit.interval:1000}")
	private Integer commitInterval;

	@Value("${ops.heartbeat.acks:all}")
	private String acks;

	@Value("${ops.heartbeat.retries:3}")
	private Integer retries;

	@Value("${ops.heartbeat.batch.size:10000}")
	private Integer batchSize;
	
	@Value("${ops.heartbeat.linger.ms:1L}")
	private Long linger;
	
	@Value("${ops.heartbeat.buffer.memory.ms:10000000}")
	private Long memory;
	
	@Value("${ops.heartbeat.sleep:5000}")
	private Long producerSleepTime;
	
	private String url;
	
	public Heartbeat() {
		startTime = Instant.now();
	}

	public Producer createProducer() {
		return this.new Producer();
	}

	public Consumer createConsumer() {
		return this.new Consumer();
	}

	public class Consumer implements Runnable {

		@Override
		public void run() {
			Properties kprops = new Properties();

			kprops.put("bootstrap.servers", getUrl());
			kprops.put("group.id", groupId);
			// NB: only "true" is valid; no code to handle "false" yet.
			kprops.put("enable.auto.commit", "true");
			kprops.put("auto.commit.interval.ms", commitInterval);
			kprops.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
			kprops.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
			KafkaConsumer<String, String> consumer = new KafkaConsumer<>(kprops);
			consumer.subscribe(Arrays.asList(topicName));

			Executors.newSingleThreadExecutor(new ThreadFactoryBuilder().setNameFormat("heartbeat.consumer").build())
					.execute(new Runnable() {
						@Override
						public void run() {
							try {
								while (true) {
									logger.trace("==> Poll for records");
									ConsumerRecords<String, String> records = consumer.poll(1000);
									logger.trace("==> Number of records: " + records.count());
									// NB: if you want the beginning ..
									// consumer.seekToBeginning(records.partitions());
									for (ConsumerRecord<String, String> record : records)
										logger.debug("==> ==> offset = {}, key = {}, value = {}", record.offset(),
												record.key(), record.value());
									Thread.sleep(sleepTime);
								}
							} catch (InterruptedException e) {
								logger.info("Heartbeat Consumer Interrupted");
							} finally {
								consumer.close();
							}
						}
					});
		}

	}

	public class Producer implements Runnable {
		@Override
		public void run() {
			/*
			 * NB: This code assumes you have kafka available at the url
			 * address. This one is available through the services in this
			 * project - ie "docker-compose up zookeeper kafka" ** You'll have
			 * to put that into /etc/hosts if running this code from CLI.
			 */
			Properties kprops = new Properties();
			kprops.put("bootstrap.servers", getUrl());
			kprops.put("acks", acks);
			kprops.put("retries", retries);	
			kprops.put("batch.size", batchSize);
			kprops.put("linger.ms", linger);
			kprops.put("buffer.memory", memory);
			kprops.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
			kprops.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

			// TODO: use ScheduledExecutorService to scheduled fixed runnable
			// https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ScheduledExecutorService.html
			// TODO: add shutdown hook, to be polite, regarding producer.close
			Executors.newSingleThreadExecutor(new ThreadFactoryBuilder().setNameFormat("heartbeat.producer").build())
					.execute(new Runnable() {
						@Override
						public void run() {
							KafkaProducer<String, String> producer = new KafkaProducer<>(kprops);
							try {
								while (true) {
									logger.trace("==> sending record");
									String now = Instant.now().toString();
									producer.send(new ProducerRecord<>(topicName, now, startTime.toString()));
									logger.debug("Sent record:  now = {}, start = {}", now, startTime);
									Thread.sleep(producerSleepTime);
								}
							} catch (InterruptedException e) {
								logger.info("Heartbeat Producer Interrupted");
							} finally {
								producer.close();
							}
						}
					});
		}
	}
	
	private String getUrl() {
		if (url != null) {
			return url;
		}
		
		url = host + ":" + port;
		
		return url;
	}

	public static void main(String[] args) throws Exception {
		Heartbeat hb = new Heartbeat();
		hb.createProducer().run();
		hb.createConsumer().run();
	}
}
